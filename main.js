var express = require("express");

var app = express();


app.get("/registration",function(req, resp){
    var applicant ={
       name: req.query.name,
       email: req.query.email,
       country: req.query.country,
       contact_num:req.query.phone,
       address:req.query.address,
       date_of_birth:req.query.birtdate,
       password:req.query.password,
       gender:req.query.gender,
    } 
    resp.status(200);
    resp.type("application/json");
    resp.json({'complete': "Submitted"});
});

app.use(express.static(__dirname + "/public"));
app.use("/lib", express.static(__dirname + "/bower_components"));

var port = parseInt(process.argv[2]) || parseInt(process.env.APP_PORT) || 3000;

app.listen(port, function() {
	console.log("Application started on port %d", port);
});
