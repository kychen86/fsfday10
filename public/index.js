//IIFE
(function() {

    var RegApp = angular.module("RegApp", []);
        
    var RegCtrl = function($http) {
        var regCtrl = this;

        regCtrl.country_list = ["Afghanistan","Albania","Algeria","Andorra","Angola","Anguilla","Antigua & Barbuda","Argentina","Armenia","Aruba","Australia","Austria","Azerbaijan","Bahamas"
		,"Bahrain","Bangladesh","Barbados","Belarus","Belgium","Belize","Benin","Bermuda","Bhutan","Bolivia","Bosnia & Herzegovina","Botswana","Brazil","British Virgin Islands"
		,"Brunei","Bulgaria","Burkina Faso","Burundi","Cambodia","Cameroon","Canada","Cape Verde","Cayman Islands","Chad","Chile","China","Colombia","Congo","Cook Islands","Costa Rica"
		,"Cote D Ivoire","Croatia","Cruise Ship","Cuba","Cyprus","Czech Republic","Denmark","Djibouti","Dominica","Dominican Republic","Ecuador","Egypt","El Salvador","Equatorial Guinea"
		,"Estonia","Ethiopia","Falkland Islands","Faroe Islands","Fiji","Finland","France","French Polynesia","French West Indies","Gabon","Gambia","Georgia","Germany","Ghana"
		,"Gibraltar","Greece","Greenland","Grenada","Guam","Guatemala","Guernsey","Guinea","Guinea Bissau","Guyana","Haiti","Honduras","Hong Kong","Hungary","Iceland","India"
		,"Indonesia","Iran","Iraq","Ireland","Isle of Man","Israel","Italy","Jamaica","Japan","Jersey","Jordan","Kazakhstan","Kenya","Kuwait","Kyrgyz Republic","Laos","Latvia"
		,"Lebanon","Lesotho","Liberia","Libya","Liechtenstein","Lithuania","Luxembourg","Macau","Macedonia","Madagascar","Malawi","Malaysia","Maldives","Mali","Malta","Mauritania"
		,"Mauritius","Mexico","Moldova","Monaco","Mongolia","Montenegro","Montserrat","Morocco","Mozambique","Namibia","Nepal","Netherlands","Netherlands Antilles","New Caledonia"
		,"New Zealand","Nicaragua","Niger","Nigeria","Norway","Oman","Pakistan","Palestine","Panama","Papua New Guinea","Paraguay","Peru","Philippines","Poland","Portugal"
		,"Puerto Rico","Qatar","Reunion","Romania","Russia","Rwanda","Saint Pierre & Miquelon","Samoa","San Marino","Satellite","Saudi Arabia","Senegal","Serbia","Seychelles"
		,"Sierra Leone","Singapore","Slovakia","Slovenia","South Africa","South Korea","Spain","Sri Lanka","St Kitts & Nevis","St Lucia","St Vincent","St. Lucia","Sudan"
		,"Suriname","Swaziland","Sweden","Switzerland","Syria","Taiwan","Tajikistan","Tanzania","Thailand","Timor L'Este","Togo","Tonga","Trinidad & Tobago","Tunisia"
		,"Turkey","Turkmenistan","Turks &amp; Caicos","Uganda","Ukraine","United Arab Emirates","United Kingdom","United States","United States Minor Outlying Islands","Uruguay","Uzbekistan","Venezuela","Vietnam","Virgin Islands (US)"
		,"Yemen","Zambia","Zimbabwe"];

        regCtrl.name = "";
        regCtrl.email = "";
        regCtrl.country = "";
        regCtrl.phone = "";
        regCtrl.address = "";
        regCtrl.birthdate = "";
        regCtrl.password = "";
        regCtrl.gender = "male";
        regCtrl.message ="";
    
        regCtrl.isValid = function(form,fieldname){
        return(form[fieldname].$invalid && form[fieldname].$dirty);
        }
        //Check birthdate
        regCtrl.validateBirthdate =function(form,birthdate){
                var birthdate = regCtrl.birthdate;
                birthdate = new Date(birthdate);
                //console.log(birthdate);
                var todaydate = new Date();
                //console.log(todaydate);
                today=todaydate.getTime();
                birth=birthdate.getTime();
                var diff=(today-birth)/(1000*60*60*24*365);
                if(diff<18){
                    //console.log("Underage");
                    return false;                    
                }
                else{
                    //console.log("Age ok");
                    return true;
                }
        }
        //Check email
        regCtrl.validateEmail = function() {
            var email = regCtrl.email;
            var x =[];
            for (var i of email) {
            x.push(i);
            }  
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
            if (!email || atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
               // console.log("Not a valid e-mail address");
                return false;
            }
            else{
                return true;
            }
        }

        //Check Contact Number
        regCtrl.validatePhone = function(){
            var phone = regCtrl.phone;
            var useable = ["0","1","2","3","4","5","6","7","8","9","+","-","(",")"," "]
            var total = 0;
            var a =[];
            for (var k of phone) {
            a.push(k);
            // console.log(a);
            }  
            //console.log("phone: " + phone.length);
            //console.log("a: " + a.length);
            for (var i =0; i<a.length; i++){
                for (var j=0; j< useable.length; j++){
                    if(a[i]===useable[j]){
                        total++;
                    break;
                    }
                }    
                //console.log("total = "+total);
            }

            if (total === phone.length){
                //console.log("valid phone number");
                total=0;
                return true
            } 
            else{
                //console.log("Invaild phone number");
                total=0;
                return false
            }
        }
        //Check Password
        regCtrl.validatePass = function(){
            var password = regCtrl.password;
            var valN = false; //Numeral
            var valL = false; //Length
            var valC = false; //Case Sensitive
            var valS = false; //Special
            var valPass = false; // check for when all val are true
            //Check Length>8
            if(!password || password.length < 8){
                //console.log("Password too short");
                valL = false;
            }
            else{
                //console.log("Length Ok");
                valL = true;
            }
            //Check Case Sensitive
            var passU = password.toUpperCase();
            //console.log(passU);
            var passD = password.toLowerCase();
            //console.log(passD);

            if(password === passD || password === passU){
                //console.log("Vary your case");
                valC = false;
            }
            else{
                valC = true;
            }

            //Check include numerals and at least one special character
            var numeral = ["0","1","2","3","4","5","6","7","8","9"]
            var special = ["@","#","S"]
            var a = [];
            for (var k of password){
            a.push(k);
            }

            for (var i =0; i<a.length; i++){
                for (var j=0; j< numeral.length; j++){
                    if(a[i]===numeral[j]){
                    valN = true;
                    //console.log("A number exist");
                    break;
                    }
                }
                for (var l=0; l< special.length; l++){
                    if(a[i]===special[l]){
                    valS = true;
                    //console.log("A special character exist");
                    break;
                    }
                }
            }
            if(valN && valS && valL && valC){
                valPass = true;
                return valPass;
            }
        }
        //Check all validation has passed
        regCtrl.check= function(){
        if(regCtrl.validateBirthdate() && regCtrl.validateEmail() && regCtrl.validatePhone() && regCtrl.validatePass()){
            return true
        }
        }
        //Submit
        regCtrl.submit = function() {
            for (var i in regCtrl){
                if (typeof (regCtrl[i]) == "string"){
                    console.log("input: %s = %s", i, regCtrl[i]);
                }
            }
            var p = $http.get("/registration", {
                params: {
                    name: regCtrl.name,
                    email: regCtrl.email,
                    country: regCtrl.country,
                    phone: regCtrl.phone,
                    address: regCtrl.address,
                    birthdate: regCtrl.birthdate,
                    password: regCtrl.password,
                    gender: regCtrl.gender 
                }
            })
            p.then(function(result) {
                regCtrl.message = result.data.complete;
            })
            p.catch(function() {
                regCtrl.message = "Cannot submit";
            })
        }

        
    };
    RegCtrl.$inject=["$http"];
    RegApp.controller("RegCtrl", RegCtrl);

})();